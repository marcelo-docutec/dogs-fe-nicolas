import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-flexbox-grid';
import '../../css/body.css'

const Error = (props) => {
  return (
  <>
    <Grid fluid>
      <Row>
        <Col xs={12}>
          {props.error[0].view === true &&
            <article className="message mar">
              <div className="message-header">
                <h1>{props.error[0].error}</h1>
              </div>
            </article>
          }
        </Col>
      </Row>
    </Grid>
  </>
)
}

Error.propTypes = {
  error: PropTypes.array
}

const mapStateToProps = state => ({
  error: state.raceReducer.viewError
})

export default connect(mapStateToProps) (Error);
