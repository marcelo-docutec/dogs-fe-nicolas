import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { filterRace } from '../../core/Race/actions';
import { connect } from 'react-redux';

class NavBar extends Component {
  searchRef = React.createRef()
  query = (event) => {
    if (event.keyCode == 13 && this.searchRef.current.value !== "") {
      this.props.filterRace(this.searchRef.current.value)
      this.searchRef.current.value = ""
    }
  }
  view = () => {
    if (this.props.viewSearch === true) {
      return (
        <div className="navbar-end">
          <div className="navbar-item">
            <div className="queryNav">
              <input
                className="input is-rounded"
                type="text"
                ref={this.searchRef}
                placeholder="¿Que raza buscas?"
                onKeyUp={this.query}
              />
            </div>
          </div>
        </div>
      )
    }
  }
  render () {
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="navbar-menu">
          <div className="navbar-start">
            <Link className="a" to="/">
              <a className="navbar-item">
                Razas de perros
              </a>
            </Link>
          </div>
          {this.view()}
        </div>
      </nav>
    )
  }
};

NavBar.propTypes = {
  race: PropTypes.string,
  viewSearch: PropTypes.bool,
  filterRace: PropTypes.func
}

const mapStateToProps = state => ({
  race: state.raceReducer.search,
  viewSearch: state.raceReducer.viewSearch
})

export default connect(mapStateToProps, {filterRace}) (NavBar);
