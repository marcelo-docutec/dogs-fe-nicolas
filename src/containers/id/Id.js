import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import '../../css/body.css';
import { connect } from 'react-redux';
// importo la accion que necesito que mute
import { viewSearch } from '../../core/Race/actions';
import { apiGetImg } from '../../core/Race/thunks';

class Id extends Component {

  componentDidMount() {
    this.props.apiGetImg(this.props.name.key)
    this.props.viewSearch(false)
  };
  render() {
    return (
      <>
      <Grid fluid>
        <Row center="xs">  
            <Col xs={12} md={8}>
            <div className="box mar card-id">
              <article className="media">
                <Col xs={7}>
                  <div className="media-left">
                    {
                      (this.props.image === "") 
                        ? <progress className="progress is-small is-primary" max="100">15%</progress> 
                        : <figure className="image image is-3by">
                            <img src={this.props.image} alt="imagen perro" className="img" />
                          </figure>
                    }
                  </div>
                </Col>
                <Col xs={5}>
                  <div className="media-content">
                    <div className="content  has-text-centered">
                      <Link to="/">
                        <span className="icon has-text-info pad">
                          <i className="fas fa-2x fa-angle-left icon-id"></i>
                        </span>
                      </Link>
                      <h1 className="title has-text-white">{this.props.name.key}</h1>
                    </div>
                  </div>
                </Col>
              </article>
            </div>
            </Col>
        </Row>
      </Grid>
    </>
    )
  }
}

Id.propTypes = {
  apiGetImg: PropTypes.func,
  viewSearch: PropTypes.func
}

const mapStateToProps = state => ({
  image: state.raceReducer.image
})
//obtiene el estado y en el mapStateToProps permito el acceso en el componente
export default connect(mapStateToProps, {apiGetImg, viewSearch}) (Id);
