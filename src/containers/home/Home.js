import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-flexbox-grid';
import Results from './components/Results';
import '../../css/body.css';

// importo conect de react-redux, siempre que se use redux se importa conect (se utiliza en el export)
import { connect } from 'react-redux';
// importo la accion que necesito que mute
import { readRace, viewSearch } from '../../core/Race/actions';
import { apiGetList } from '../../core/Race/thunks';

class Home extends Component {

  componentDidMount() {
    this.props.apiGetList()
    this.props.viewSearch(true)
  };
  render () {
    return (
      <>
        <Results />
        <Grid fluid>
          <Row center="xs" className="mar">
            {this.props.race && this.props.race.map((element, key)  => (     
              <Col xs={12} sm={6} md={4} lg={3} key={key}>
                <div className="notification cardHome">
                  <p>{element}</p>
                  <Link className="a" to={`post/${element}`}>
                    <button className="button is-primary is-rounded">Ver imagen</button>
                  </Link>
                </div>
              </Col>
            ))}
          </Row>
        </Grid>
      </>
    )
  }
};

Home.propTypes = {
  race: PropTypes.array,
  readRace: PropTypes.func,
  viewSearch: PropTypes.func,
  apiGetList: PropTypes.func
}

const mapStateToProps = state => ({
  race: state.raceReducer.race
})
//obtiene el estado y en el mapStateToProps permito el acceso en el componente
export default connect(mapStateToProps, {readRace, viewSearch, apiGetList}) (Home);


