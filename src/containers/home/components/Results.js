import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Grid, Row, Col } from 'react-flexbox-grid';
import './style.css'
import { viewError } from '../../../core/Race/actions';
import Error from '../../Error/Error'

class Results extends Component {
  state = {
    filter: null
  }
  componentDidUpdate(nextProps, nextState) {
    if (this.props.race.search !== nextProps.race.search) {
      if (this.state.filter != null) {
        console.log("hacerlo nulo")
        this.setState({
          filter: null
        })
      }
      this.props.race.race.filter(name => {
        console.log("FILTRANDO");
        if (name === this.props.race.search) {
          this.setState({
            filter: name
          })
        }
        console.log("fin del filtrado");
        
      })
      if (this.state.filter != null) {
        console.log("hacerlo nulo")
        console.log("Mostrar el error");
        this.props.viewError([{
          error: 'No pudimos encontrar esa raza',
          view: true
        }])
        setTimeout(() => {
          this.props.viewError([{
            error: '',
            view: false
          }])
        }, 1000);
      }
      
    }
  }
  render () {
    return (
      <>
      <Error />
      <Grid fluid>
        <Row>
          <Col xs={12}>
            <Row center="xs" className="mar">
              {this.state.filter && (
                <Col xs={12} sm={8} md={8} lg={10}>
                  <div className="divider"></div>
                  <div className="notification cardHome">
                    <p>{this.state.filter}</p>
                    <Link className="a" to={`post/${this.state.filter}`}>
                      <button className="button is-primary is-rounded">Ver imagen</button>
                    </Link>
                  </div>
                  <div className="divider"></div>
                </Col>
              )}
            </Row>
          </Col>
        </Row>
      </Grid>
      </>
    )
  }
};

Results.propTypes = {
  race: PropTypes.object,
  filter: PropTypes.array,
  viewError: PropTypes.func
}

const mapStateToProps = state => ({
  race: state.raceReducer
})

export default connect(mapStateToProps, {viewError}) (Results);
