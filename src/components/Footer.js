import React from 'react';

const Footer = () => {
  return (
    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          Proyecto creado con <strong>Bulma</strong> Por  
          <a href="https://nicolasgonzalezprogramador.firebaseapp.com" target="_blank" > Nicolas Gonzalez</a>
        </p>
      </div>
    </footer>
  )
};

export default Footer;
